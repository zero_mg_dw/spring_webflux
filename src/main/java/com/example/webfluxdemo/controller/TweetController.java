package com.example.webfluxdemo.controller;

import com.example.webfluxdemo.exception.TweetNotFoundException;
import com.example.webfluxdemo.model.Tweet;
import com.example.webfluxdemo.payload.ErrorResponse;
import com.example.webfluxdemo.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ReplayProcessor;

import javax.validation.Valid;
import java.time.Duration;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

/**
 * Created by rajeevkumarsingh on 08/09/17.
 */
@RestController
public class TweetController {


    // broadcaster: notificador a todos los suscritos (es decir... todos los clientes)
    private ReplayProcessor<ServerSentEvent<Tweet>> replayProcessor =
            ReplayProcessor.<ServerSentEvent<Tweet>>create();

    @Autowired
    private TweetRepository tweetRepository;


    @GetMapping("/tweets")
    @CrossOrigin
    public Flux<Tweet> getAllTweets() {

        return tweetRepository.findAll().delayElements(Duration.ofMillis(1000));
    }

    /**
     * Servicio REST encargado de retorno de stream, este servicio esta conectado de forma reactiva obteniendo los
     * cambios de la BD  de forma directa sobre cualquier cambio.....
     *
     * @return
     */
    @GetMapping(value = "/tweets/reactorDriver",
            produces = TEXT_EVENT_STREAM_VALUE)
    @CrossOrigin
    public Flux<Tweet> getFromReactorDriver() {
        return tweetRepository.findWithTailableCursorBy()
                .delayElements(Duration.ofMillis(1000));
    }


    /**
     * Servicio REST cuyo proposito es unicamente proveer del canal  de suscripción que recibirá cualqueir notificación
     * del broadcaster
     *
     * @return
     */
    @GetMapping("/tweets/receiveFromStream")
    public Flux<ServerSentEvent<Tweet>> send() {

        return replayProcessor.log();
    }


    /**
     * Servicio REST cuyo propósito es únicamente ingresar datos en la BD, y justo en el momento es que es ingresado el dato,
     * este servicio notificará al broadcaster sobre un nuevo cambio.
     *
     * @param tweet
     * @return
     */
    @PostMapping("/tweets/sendToStream")
    public Mono<Tweet> createTweetsFromStream(
            @Valid @RequestBody Tweet tweet
    ) {
        return tweetRepository.save(tweet).map(tweetSaved -> {
            System.out.println("Result save from Server:" + tweetSaved.getId());
            replayProcessor.onNext(ServerSentEvent.builder(tweetSaved).build());
            return tweet;
        });

        //replayProcessor.onNext(ServerSentEvent.builder(resultSave.).build());
        //return null;
    }


    @PostMapping("/tweets")
    public Mono<Tweet> createTweets(
            @Valid @RequestBody Tweet tweet
    ) {
        return tweetRepository.save(tweet);
    }


    @GetMapping("/tweets/{id}")
    public Mono<ResponseEntity<Tweet>> getTweetById(@PathVariable(value = "id") String tweetId) {
        return tweetRepository.findById(tweetId)
                .map(savedTweet -> ResponseEntity.ok(savedTweet))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/tweets/{id}")
    public Mono<ResponseEntity<Tweet>> updateTweet(@PathVariable(value = "id") String tweetId,
                                                   @Valid @RequestBody Tweet tweet) {
        return tweetRepository.findById(tweetId)
                .flatMap(existingTweet -> {
                    existingTweet.setText(tweet.getText());
                    return tweetRepository.save(existingTweet);
                })
                .map(updateTweet -> new ResponseEntity<>(updateTweet, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/tweets/{id}")
    public Mono<ResponseEntity<Void>> deleteTweet(@PathVariable(value = "id") String tweetId) {
        return tweetRepository.findById(tweetId)
                .flatMap(existingTweet ->
                        tweetRepository.delete(existingTweet)
                                .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)))
                )
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // Tweets are Sent to the client as Server Sent Events
    @GetMapping(value = "/stream/tweets", produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<Tweet> streamAllTweets() {
        return tweetRepository.findAll();
    }




    /*
        Exception Handling Examples (These can be put into a @ControllerAdvice to handle exceptions globally)
    */

    @ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Tweet with the same text already exists"));
    }

    @ExceptionHandler(TweetNotFoundException.class)
    public ResponseEntity handleTweetNotFoundException(TweetNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
