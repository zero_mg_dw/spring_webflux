package com.example.webfluxdemo.controller;

import com.example.webfluxdemo.model.Person;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.ReplayProcessor;

import java.time.Duration;


@RestController
public class SseController {

    private ReplayProcessor<ServerSentEvent<String>> replayProcessor = ReplayProcessor.<ServerSentEvent<String>>create();

    @GetMapping("/sse/string")
    @CrossOrigin
    Flux<String> string() {
        return Flux
                .interval(Duration.ofSeconds(1))
                .map(l -> "foo " + l);
    }

    @GetMapping("/sse/person")
    @CrossOrigin
    Flux<ServerSentEvent<Person>> person() {
        return Flux
                .interval(Duration.ofSeconds(1))
                .map(l -> {
                    System.out.println("on server.... creating person...");
                    return ServerSentEvent.builder(new Person(Long.toString(l), "foo", "bar")).build();
                });
    }

    @GetMapping("/sse/event")
    Flux<ServerSentEvent<String>> event() {
        return Flux
                .interval(Duration.ofSeconds(1))
                .map(l -> ServerSentEvent
                        .builder("foo\nbar")
                        .comment("bar\nbaz")
                        .id(Long.toString(l))
                        .build());
    }

    @PostMapping("/sse/send/{val}")
    public void receive(@PathVariable("val") String s) {

        replayProcessor.onNext(ServerSentEvent.builder(s).build());
    }

    @GetMapping("/sse/receive")
    public Flux<ServerSentEvent<String>> send() {

        return replayProcessor.log("playground");
    }

}